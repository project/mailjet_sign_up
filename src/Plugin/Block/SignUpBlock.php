<?php

namespace Drupal\mailjet_sign_up\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * SignUpBlock.
 *
 * @Block(
 * id = "mailjet_sign_up",
 * admin_label = @Translation("Mailjet sign up"),
 * )
 */
class SignUpBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Construct of SignUpBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->formBuilder->getForm('Drupal\mailjet_sign_up\Form\SignUpForm', $this->getConfiguration());
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'header' => $this->t('Subscribe to our newsletter'),
      'placeholder' => $this->t('Your email address'),
      'api_key' => '',
      'secret_key' => '',
      'contact_list_id' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['header'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Header'),
      '#description' => $this->t('This text will appear upon the email field.'),
      '#default_value' => $this->configuration['header'],
    ];
    $form['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#description' => $this->t('This text will appear in the email field when empty.'),
      '#default_value' => $this->configuration['placeholder'],
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mailjet API key'),
      '#description' => $this->t('See https://app.mailjet.com/account/api_keys.'),
      '#default_value' => $this->configuration['api_key'],
    ];
    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mailjet secret key'),
      '#description' => $this->t('See https://app.mailjet.com/account/api_keys.'),
      '#default_value' => $this->configuration['secret_key'],
    ];
    $form['contact_list_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mailjet contact list ID'),
      '#description' => $this->t('See https://app.mailjet.com/contacts.'),
      '#default_value' => $this->configuration['contact_list_id'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $config_key = [
      'header',
      'placeholder',
      'api_key',
      'secret_key',
      'contact_list_id',
    ];

    foreach ($config_key as $config_name) {
      $this->configuration[$config_name] = trim($form_state->getValue($config_name));
    }
  }

}
