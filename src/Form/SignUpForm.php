<?php

namespace Drupal\mailjet_sign_up\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Mailjet\Client;
use Mailjet\Resources;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SignUpForm.
 *
 * @package Drupal\mailjet_sign_up\Form
 */
class SignUpForm extends FormBase {

  /**
   * Messenger service variable.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * SignUpForm constructor.
   *
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger service variable.
   */
  public function __construct(Messenger $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailjet_sign_up_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $block_config = $this->getBlockConfig($form_state);

    $form['header'] = ['#markup' => '<span class="intro">' . $block_config['header'] . '</span>'];
    $form['mail'] = [
      '#type' => 'textfield',
      '#attributes' => ['placeholder' => $block_config['placeholder']],
      '#default_value' => '',
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sign up'),
    ];
    return $form;
  }

  /**
   * Get config of block.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return mixed
   *   Block config.
   */
  protected function getBlockConfig(FormStateInterface $form_state) {
    // @see SignUpBlock::build().
    return $form_state->getBuildInfo()['args'][0];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get the list ID.
    $block_config = $this->getBlockConfig($form_state);
    $response = $this->subscribe($block_config['contact_list_id'], $form_state);
    if ($response->success()) {

      $this->messenger->addMessage($this->t('You successfully signed up to our newsletter. Thank you!'));
      return;
    }

    $this->messenger->addMessage($this->t('Sorry, your sign-up failed.'), 'error');
    if ($response->getReasonPhrase()) {
      $this->messenger->addMessage($this->t("Here's the error message from Mailjet subscription manager: %error", ['%error' => $response->getReasonPhrase()]), 'error');
    }
  }

  /**
   * Subscribe to a list id.
   *
   * @param string $list_id
   *   Id of contact list.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Mailjet\Response
   *   Mailjet response.
   */
  protected function subscribe($list_id, FormStateInterface $form_state) {
    $data = $this->buildSubscriptionData($list_id, $form_state);
    return $this->getMailjetClient($this->getBlockConfig($form_state))
      ->post(Resources::$ContactslistManagecontact, $data);
  }

  /**
   * Build data for Mailjet request.
   *
   * @param string $list_id
   *   Id of contact list.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Data of the Mailjet request.
   */
  protected function buildSubscriptionData($list_id, FormStateInterface $form_state) {
    return [
      'id' => $list_id,
      'body' => [
        'Email' => $form_state->getValue('mail'),
        'Action' => 'addforce',
      ],
    ];
  }

  /**
   * Get Mailjet Client.
   *
   * @param array $block_config
   *   Block config.
   *
   * @return \Mailjet\Client
   *   Mailjet client.
   */
  protected function getMailjetClient(array $block_config) {
    return new Client($block_config['api_key'], $block_config['secret_key']);
  }

}
